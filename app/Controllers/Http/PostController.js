"use strict";

// get post model
const Post = use("App/Models/Post");

// load validator
const { validate } = use("Validator");

class PostController {
  async index({ view }) {
    const posts = await Post.all();
    return view.render("posts.index", {
      title: "All Post List",
      posts: posts.toJSON()
    });
  }

  async details({ params, view }) {
    const post = await Post.find(params.id);
    return view.render("posts.details", { post: post.toJSON() });
  }

  async add({ view }) {
    return view.render("posts.add");
  }

  async editPost({ params, view }) {
    const post = await Post.find(params.id);

    return view.render("posts.edit", { post: post });
  }

  async update({ params, request, response, session }) {
    const validation = await validate(request.all(), {
      title: "required|min:3|max:255",
      body: "required|min:3|max:255"
    });

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll();
      return response.redirect("back");
    }

    const post = await Post.find(params.id);
    post.title = request.input("title");
    post.body = request.input("body");

    await post.save();

    session.flash({ notification: "Post Updated!" });

    return response.redirect("/posts");
  }

  async deletePost({ params, session, response }) {
    const post = await Post.find(params.id);

    await post.delete();

    session.flash({ notification: "Post Deleted !" });

    return response.redirect("/posts");
  }

  async save({ request, response, session }) {
    const validation = await validate(request.all(), {
      title: "required|min:3|max:255",
      body: "required|min:3|max:255"
    });

    if (validation.fails()) {
      session.withErrors(validation.messages()).flashAll();
      return response.redirect("back");
    }

    // create a post object
    const post = new Post();

    // get form data.
    post.title = request.input("title");
    post.body = request.input("body");

    // save data to the database.
    await post.save();

    // set flash message.
    session.flash({ notification: "Post Added!" });

    return response.redirect("/posts");
  }
}

module.exports = PostController;
